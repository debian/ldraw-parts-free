#!/bin/sh

set -ex

VERSION=$2
UNPACKDIR=$(mktemp -d)

tar --directory "${UNPACKDIR}" --extract --file "../ldraw-parts-free_$VERSION.orig.tar.xz"

# recover mklist.c and headers from mklist1_6.zip
unzip -q "${UNPACKDIR}/ldraw/mklist1_6.zip" mklist.c include/*.h -d "${UNPACKDIR}/ldraw"
rm "${UNPACKDIR}/ldraw/mklist1_6.zip"

# repack source tarball, removing CC-BY-2.0 material from it
env --chdir "${UNPACKDIR}/ldraw" python3 "$(pwd)/debian/gendfsgsrc.py" "$VERSION"
mv "${UNPACKDIR}/ldraw-parts-free_$VERSION.orig.tar.xz" ../ldraw-parts-free_$VERSION+dfsg.orig.tar.xz
rm -r "$UNPACKDIR"

exit 0
