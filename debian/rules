#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# for cross-build aware CC and CC_FOR_BUILD
include /usr/share/dpkg/buildtools.mk

# only necessary until dpkg provides this
CFLAGS_FOR_BUILD ?= $(shell   dpkg-architecture --host-arch $(DEB_BUILD_ARCH) --force --command dpkg-buildflags --get CFLAGS)
CPPFLAGS_FOR_BUILD ?= $(shell dpkg-architecture --host-arch $(DEB_BUILD_ARCH) --force --command dpkg-buildflags --get CPPFLAGS)
LDFLAGS_FOR_BUILD ?= $(shell  dpkg-architecture --host-arch $(DEB_BUILD_ARCH) --force --command dpkg-buildflags --get LDFLAGS)

%:
	dh $@

execute_after_dh_auto_build:
	# compile host architecture version to be included in ldraw-mklist package
	$(CC) -o mklist $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -I./include mklist.c
	# compile build architecture version to run during the build
	# this is only needed to support cross-compilation
	# in the native case, this is identical to the above
	$(CC_FOR_BUILD) -o mklist-native $(CFLAGS_FOR_BUILD) $(CPPFLAGS_FOR_BUILD) $(LDFLAGS_FOR_BUILD) -I./include mklist.c
	./mklist-native -i parts -d

execute_after_dh_auto_install:
	mkdir -p debian/tmp/usr/bin debian/tmp/usr/share/ldraw
	cp mklist debian/tmp/usr/bin/ldraw-mklist
	cp -r LDCfgalt.ldr LDConfig.ldr LDConfig_TLG.ldr parts models p debian/tmp/usr/share/ldraw/

execute_after_dh_auto_clean:
	rm -f mklist mklist-native parts.lst
